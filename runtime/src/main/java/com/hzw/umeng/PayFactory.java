package com.hzw.umeng;

import com.hzw.umeng.entity.Configs;
import com.hzw.umeng.pay.WeChatPay;
import com.hzw.umeng.utils.Utils;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

/**
 * @author: HZWei
 * @date: 2021/11/4
 * @desc:
 */
@Deprecated
class PayFactory {
    public static <T extends PayHelper.Builder>PayHelper.Builder <T>build(T t)   {
        if (t instanceof PayHelper.WXBuilder) {
            return (PayHelper.Builder<T>) t;
        }else if (t instanceof PayHelper.AliBuilder){
            return (PayHelper.Builder<T>) t;
        }
        return null;
    }

} 
